class Swiper {
  private ul: HTMLElement | null
  private readonly imgList: Array<HTMLElement>
  private circleList: Array<HTMLElement>
  private readonly imgListLength: number
  private timer: number | undefined
  private readonly liList: any;

  public constructor(container: string) {
    this.ul = document.querySelector(container);
    this.liList = this.ul?.getElementsByTagName('li')
    this.imgListLength = this.liList.length
    this.imgList = []
    this.circleList = []
    this.createImgList(this.liList)
    this.createBorderIcon()
    this.createBottomCircle()
    this.handleBottomCircleStyle(this.getCurrentSliderId())
    this.render()
    this.autoSlideSwiper()
  }

  private createBorderIcon = (): void => {
    let preIcon: HTMLImageElement | null = document.querySelector('#preIcon');
    let nextIcon: HTMLImageElement | null = document.querySelector('#nextIcon');
    preIcon?.addEventListener('click', () => {
      this.stopAutoSlideSwiper()
      this.goPre()
      this.autoSlideSwiper()
    })
    nextIcon?.addEventListener('click', () => {
      this.stopAutoSlideSwiper()
      this.goNext()
      this.autoSlideSwiper()
    })
  }
  private createImgList = (list: any): void => {
    for (let i = 0; i < list.length; i++) {
      list[i].addEventListener('mouseover', () => {
        this.stopAutoSlideSwiper()
      })
      list[i].addEventListener('mouseleave', () => {
        this.autoSlideSwiper()
      })
      list[i].dataset.id = i.toString()
      this.imgList?.push(list[i])
    }
  }
  private handleBottomCircleStyle = (id: string): void => {
    this.circleList.forEach(item => {
      item.style.background = item.dataset.id === id ? 'deepskyblue' : 'aliceblue'
    })
  }
  private eventLoop = (callback: () => void, id: string): void => {
    while (true) {
      callback()
      if (id === this.getCurrentSliderId()) {
        break
      }
    }
  }
  private gotoTargetItem = (id: string): void => {
    if (id < this.getCurrentSliderId()) {
      this.eventLoop(this.goPre, id)
    } else if (id > this.getCurrentSliderId()) {
      this.eventLoop(this.goNext, id)
    }
  }
  private getCurrentSliderId = (): any => this.imgList[this.imgListLength - 2].dataset.id
  private createBottomCircle = (): void => {
    let div = document.querySelector('#swiperFooter');
    let spanList: any = div?.getElementsByTagName('span')
    for (let i = 0; i < spanList.length; i++) {
      spanList[i].dataset.id = (this.imgListLength - 2 - i < 0 ? this.imgListLength - 1 : this.imgListLength - 2 - i).toString()
      spanList[i].addEventListener('click', () => {
        this.stopAutoSlideSwiper()
        this.gotoTargetItem(spanList[i].dataset.id)
        this.autoSlideSwiper()
      })
      this.circleList?.push(spanList[i])
    }
  }
  private render = (): void => {
    this.imgList[this.imgListLength - 3].style.left = '0px'
    this.imgList[this.imgListLength - 1].style.left = '400px'
    Object.assign(this.imgList[this.imgListLength - 2].style, {
      left: '200px',
      zIndex: '100',
      transform: 'scale(1.3)'
    })
  }
  private resetStyle = (): void => {
    this.imgList.forEach((item, index) => {
      Object.assign(this.imgList[index].style, {
        zIndex: index.toString(),
        transform: 'scale(1)'
      })
    })
  }
  private goNext = (): void => {
    let lastItem: HTMLElement | undefined = this.imgList.pop()
    if (lastItem) {
      this.imgList?.unshift(lastItem)
    }
    this.resetStyle()
    this.render()
    this.handleBottomCircleStyle(<string>this.getCurrentSliderId())
  }
  private goPre = (): void => {
    let firstItem: HTMLElement | undefined = this.imgList.shift()
    if (firstItem) {
      this.imgList?.push(firstItem)
    }
    this.resetStyle()
    this.render()
    this.handleBottomCircleStyle(this.getCurrentSliderId())
  }
  private autoSlideSwiper = (): void => {
    this.timer = window.setInterval(this.goNext, 3000)
  }
  private stopAutoSlideSwiper = (): void => {
    clearInterval(this.timer)
  }
}

export {
  Swiper
}
