const debounce = (callback: Function, time: number) => {
  let timer: number | null = null
  return () => {
    if (timer) {
      window.clearTimeout(timer)
    }
    timer = window.setTimeout(callback, time)
  }
}
export {
  debounce
}
