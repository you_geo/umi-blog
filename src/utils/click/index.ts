import {Sketch} from './sketch'

class Particle {
  private alive: boolean = false
  private radius: number = 0
  private wander: number = 0
  private theta: any
  private drag: number = 0
  private color: string = ''
  private x: number = 0
  private y: number = 0
  private vx: number = 0
  private vy: number = 0

  constructor(x?: number, y?: number, radius?: number) {
    this.init(x, y, radius)
  }

  init(x?: number, y?: number, radius?: number) {
    this.alive = true;
    this.radius = radius || 10;
    this.wander = 0.15;
    this.theta = Sketch.random(Math.PI * 2);
    this.drag = 0.92;
    this.color = '#ffeb3b';

    this.x = x || 0.0;
    this.y = y || 0.0;
    this.vx = 0.0;
    this.vy = 0.0;
  }

  move() {
    this.x += this.vx;
    this.y += this.vy;
    this.vx *= this.drag;
    this.vy *= this.drag;
    this.theta += Sketch.random(-0.5, 0.5) * this.wander;
    this.vx += Math.sin(this.theta) * 0.1;
    this.vy += Math.cos(this.theta) * 0.1;
    this.radius *= 0.96;
    this.alive = this.radius > 0.5;
  }

  draw(ctx: HTMLCanvasElement | any) {
    ctx.beginPath();
    ctx.arc(this.x as number, this.y as number, this.radius as number, 0, Math.PI * 2);
    ctx.fillStyle = this.color;
    ctx.fill();
  }
}

class clickHandler {
  private readonly MAX_PARTICLES: number;
  private readonly COLOURS: string[];
  private readonly particles: any[];
  private pool: any[];
  private readonly clickparticle: any;

  constructor() {
    this.MAX_PARTICLES = 50;
    //圆点颜色库
    this.COLOURS = ["#5ee4ff", "#f44033", "#ffeb3b", "#F38630", "#FA6900", "#f403e8", "#F9D423"];
    this.particles = [];
    this.pool = [];
    this.clickparticle = Sketch.create({
      container: document.getElementById('clickCanvas')
    });
    this.clickparticle.spawn = (x: number, y: number) => {
      if (this.particles.length >= this.MAX_PARTICLES)
        this.pool.push(this.particles.shift());
      let particle = this.pool.length ? this.pool.pop() : new Particle();
      particle.init(x, y, Sketch.random(5, 20));//圆点大小范围
      particle.wander = Sketch.random(0.5, 2.0);
      particle.color = Sketch.random(this.COLOURS);
      particle.drag = Sketch.random(0.9, 0.99);
      let theta = Sketch.random(Math.PI * 2);
      let force = Sketch.random(1, 5);
      particle.vx = Math.sin(theta) * force;
      particle.vy = Math.cos(theta) * force;
      this.particles.push(particle);
    };
    this.clickparticle.update = () => {
      let i, particle;
      for (i = this.particles.length - 1; i >= 0; i--) {
        particle = this.particles[i];
        if (particle.alive)
          particle.move();
        else
          this.pool.push(this.particles.splice(i, 1)[0]);
      }
    };
    this.clickparticle.draw = () => {
      this.clickparticle.globalCompositeOperation = 'lighter';
      for (let i = this.particles.length - 1; i >= 0; i--) {
        this.particles[i].draw(this.clickparticle);
      }
    };
    //按下时显示效果，mousedown 换成 click 为点击时显示效果（我用的 click）
    document.addEventListener("mousedown", (e: MouseEvent) => {
      let max, j;
      //排除一些元素
      // @ts-ignore
      "TEXTAREA" !== e.target?.nodeName && "INPUT" !== e.target?.nodeName && "A" !== e.target?.nodeName && "I" !== e.target?.nodeName && "IMG" !== e.target?.nodeName
      && (() => {
        for (max = Sketch.random(15, 20), j = 0; j < max; j++)
          this.clickparticle.spawn(e.clientX, e.clientY);
      })();
    });
  }
}

export {
  clickHandler
}
