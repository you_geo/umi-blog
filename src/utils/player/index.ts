import 'aplayer/dist/APlayer.min.css';
import APlayer from 'aplayer';

const musicPlayer = (element: string) => {
  const options = {
    container: document.querySelector(element),
    fixed: true,
    mini: true,
    autoplay: true,
    lrcType: 3,
    audio: [
      {
        "name": "初恋的朦胧诗",
        "artist": "房东的猫",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=00438xif00OYot&auth=68c3d61efe8361a2a774b1c953d3bed39a7d8535",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=002sw36F1R2PK1&auth=2506b40cf93cafe5e14b05a09b804b407a09bf14",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=00438xif00OYot&auth=f4ad176bc2a86556fa91931e44e66b8f9f4056d8"
      },
      {
        "name": "是初恋是心动是你",
        "artist": "房东的猫",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=003JUlF24EFntI&auth=75d9f6b4a11cbff5664b00b975ce74b4f6d0e899",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=0032wm9x0VO0VG&auth=27e9ca1f37fd0a3a5fda8a73d18e944a356e258c",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=003JUlF24EFntI&auth=ee4604c68287fea8c4e57e0907e7615ec45cef03"
      },
      {
        "name": "一百个不喜欢你的方法",
        "artist": "房东的猫",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=0004uQJi1QN0Mf&auth=eb9f8c815e754af1dd16c7b1be4c32ab8bdf6d79",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=0048VMT329wFfD&auth=3dc95b970cee5bdcf0b39260758e41d10452f3b5",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=0004uQJi1QN0Mf&auth=7470cfbcb2f33ff7b092f94920fadd620f5a77df"
      },
      {
        "name": "锦瑟",
        "artist": "房东的猫",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=0035Y5hZ1dlLrm&auth=1921f3b75f6a3f40c50a35fd20f6892d65017ea0",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=002DzQ632vXrHN&auth=b5a7adb5d472a295ffe18c8d5da3bf6813be258e",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=0035Y5hZ1dlLrm&auth=ad4142e6d68d517c93d0d9878406c41d709b2ed3"
      },
      {
        "name": "云烟成雨",
        "artist": "房东的猫",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=001yYM0I30CzdP&auth=243686f98a14224f0d462fb75e9a3dfe3f3d2b12",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=004NFJ230yX0Nz&auth=f68522433cb19f7cf34ce99cb9cf7c2ba76ce5a9",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=001yYM0I30CzdP&auth=0b75a8e1f5dbfaddd65cef905563e0b80a162cb2"
      },
      {
        "name": "New Boy ",
        "artist": "房东的猫",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=003UTVCN0QvffG&auth=156d2d64cdb31afd08c9a1feedd8d07085736a2d",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=002uI8vg4QBFEY&auth=9f09c0d1325eb3f220f118a9dd4cec0ed2eff0ed",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=003UTVCN0QvffG&auth=ded885ebcfdc2c2aea7a3c75f74ad9d64579dda8"
      },
      {
        "name": "我可以",
        "artist": "房东的猫",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=0039o8U140hL15&auth=ce432cb5e81860f126ac3622c94c721f10a84c6f",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=003oCARp0Nlbpy&auth=ad98b452b6d94faa74eef6e0a9c88f7d45a4ecdf",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=0039o8U140hL15&auth=035b0fccdde0ed3eabab41a65b387788a032f292"
      },
      {
        "name": "下一站，茶山刘",
        "artist": "安来宁 / 房东的猫",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=000gYjUp2aHqed&auth=946b019ad6a6d2bcfad265d7f5c99c0c9e97c6c3",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=000Q0Sh21WrCKD&auth=3fa0686d77a1af5e55816df4bcedb0bf78a4fe36",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=000gYjUp2aHqed&auth=dc9cb4e03e40b0fb9fd10c7203cfc846c20b6fc8"
      },
      {
        "name": "rainy night/雨夜 (Ft. 房东的猫)",
        "artist": "FACEVOID桃心脸哥 / 房东的猫",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=002bOExX3qddJh&auth=061310a33fc5953594c1811d1f4a20269891cee9",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=003to2hv1xas8F&auth=cfaf695c66bcb5ddee8a15392490603a0dbb95fd",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=002bOExX3qddJh&auth=cdb505cb93aeaa2493a6482c8bfb04be4a897eda"
      },
      {
        "name": "斑马斑马 (翻唱)",
        "artist": "房东的猫",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=003NfW9d3lhFUE&auth=9cffad58f836a2c42c826b5fcf889ad209f26553",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=001I9nkw1I0NTt&auth=fec0903d2fd407907612c5b9e65e9a17ff55954c",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=003NfW9d3lhFUE&auth=5b95438818b02f16b0720465bdfde64809be89ae"
      },
      {
        "name": "我是任何野蛮生长的事物",
        "artist": "房东的猫",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=003fyuuV0TD8v5&auth=7298c50added1d2678215b1f4c560c97767f706b",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=001hlNSm3W2JyL&auth=e3a05ea226e8162f1d79335c33ddfbdc03920696",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=003fyuuV0TD8v5&auth=8ee5bf919cddd8931d4659dba2a4b2c1942c5116"
      },
      {
        "name": "蝴蝶之夏",
        "artist": "房东的猫",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=001q3yaN0SVDTP&auth=0fc512e6ef1084f7d8162300c11083a932a5896b",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=003w1jij2XuU3Q&auth=be751cc049b5a031faf829d1528934d97d0d6b42",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=001q3yaN0SVDTP&auth=52cdd4624c862a5c9092d3c4176c15cd864d5f3e"
      },
      {
        "name": "时光，一如既往",
        "artist": "房东的猫",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=002j31BN4azXch&auth=09c045a2b0575145ed831d5334c16eda773a1d9d",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=003VmuMe2FFMRz&auth=c74925f16f68bdfec47346adaf8da027f4da2024",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=002j31BN4azXch&auth=74fc7a64e685bbd8141236de84a66a0d0370f907"
      },
      {
        "name": "青梅",
        "artist": "房东的猫",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=000NpqaE2fmEhv&auth=adc8e8d3a615d4e0ef3532fce562aec18118e5cb",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=0031tqaN3EHiZU&auth=933c9bd604fb727b4063710baea0735faa56d8e7",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=000NpqaE2fmEhv&auth=c20d0f0ce3e70e20c869c2bef3685a46257865f1"
      },
      {
        "name": "至此",
        "artist": "房东的猫",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=003URKUK44ovcP&auth=c907991390c66378f026a2122dd4e235da54b003",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=004b0rFW16tU3N&auth=05e4961686153d0b031e8db4cc034880f0a9e5be",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=003URKUK44ovcP&auth=78c5ed6be3ac58264f13ac9f107a6fa63c1d677e"
      },
      {
        "name": "那些花儿",
        "artist": "房东的猫 / 谢春花",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=001vV6k03uY2Wu&auth=84c858ff48d27863d6f4d3691128d5f6616afa87",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=003sd6L32TqqtR&auth=05c9345c3d81f0c8da7d030ed3d827686057b404",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=001vV6k03uY2Wu&auth=9e1d8dd63aaee0a766c77463dba60e4f6d5231e2"
      },
      {
        "name": "给妈妈",
        "artist": "房东的猫",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=003YgdI53s1Nv4&auth=2c3e3d7481f71cefc18e0a79511399a191f60b38",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=003F3RMe0EtpFb&auth=dc2db8562c0e1f41ba624109dd7380738893275a",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=003YgdI53s1Nv4&auth=e6a283d70d33cfaa55a0fd541e5450ad6d13ce04"
      },
      {
        "name": "月亮鲸鱼",
        "artist": "房东的猫 / 陆宇鹏",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=003UAKjC1VVaII&auth=047f7eb792cdcd2b7bbe06e95a41bb165eb4a1b4",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=001xPKFT4Kds8v&auth=1440488330750e7299bfb173bebffe4783f3ca91",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=003UAKjC1VVaII&auth=1dd3377b91e73130beaa4a68443c54bec067cd75"
      },
      {
        "name": "美好事物-再遇少年",
        "artist": "边程 / 房东的猫",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=001HNpRx3JGqG9&auth=b44d2c7622cb21e3093d0ac1912f71c1512e70f3",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=000CmN3V3jH91C&auth=762d0eb0e03485f9c6c618586562098470d6085d",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=001HNpRx3JGqG9&auth=f4cad5add57ac4e94de9d32163168a71f88b3a1a"
      },

      {
        "name": "颠倒",
        "artist": "房东的猫",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=003njx7I3cTj5f&auth=32a29488044b5f8326f88e01adcb5f50ea12d312",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=002VrNqW4ROUHz&auth=8c708b7aabf93bd823b3202e4b074f78e59a778f",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=003njx7I3cTj5f&auth=17b424812a1ef480e2cf28a12f620ada2273903d"
      },

      {
        "name": "远在咫尺的爱",
        "artist": "房东的猫",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=001V6auS3nwejD&auth=2e1f4d75ff13397fa5ac87bb525b938e20c8a0ca",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=000o6A2x0Mo6O4&auth=e6b6a4e6b564714f2d8df7e399bf6df92b910374",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=001V6auS3nwejD&auth=bc6888aa88bf7075b94345d32a76970b63483562"
      },
      {
        "name": "秋又来了",
        "artist": "孟大宝 / 房东的猫",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=001iY8Kx3zqYSE&auth=5da8659766d0c2581e61dff00b60f2b0e96ef210",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=0001RU7W2XQ1xK&auth=f5a5ae934a4280e394ca5a026e52a806915ef09d",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=001iY8Kx3zqYSE&auth=d6ea96ed374498beea54527346e28485afa5e2bc"
      },
      {
        "name": "银河迪厅",
        "artist": "房东的猫",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=001OePWl0x9vgD&auth=418e0833a9acdeffcb4002846a65193dedc751aa",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=00332HNk3ic7CI&auth=4da47a6afd49cc204a83dff050ee8ea0f63faffa",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=001OePWl0x9vgD&auth=5e08e51497aa108e2ffbb60fb42589cf9041d69f"
      },
      {
        "name": "难得",
        "artist": "安来宁 / 房东的猫",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=003NCHMz2V7mhj&auth=807bcd42c4dafdb6c63f436005abb0d338ca66a6",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=000Q0Sh21WrCKD&auth=3fa0686d77a1af5e55816df4bcedb0bf78a4fe36",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=003NCHMz2V7mhj&auth=bb1b1ab9847a3f8b94143d1bfd6b967f071898e1"
      },
      {
        "name": "New Boy",
        "artist": "房东的猫",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=001R5CHU1QaHrZ&auth=7ed7725b347696e5b83b546f7b2a5e15a6529944",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=002uI8vg4QBFEY&auth=9f09c0d1325eb3f220f118a9dd4cec0ed2eff0ed",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=001R5CHU1QaHrZ&auth=206a6ad4a746277e3ad009f779ef34265f03d8b9"
      },
      {
        "name": "real me",
        "artist": "房东的猫",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=004bAPw11eyb71&auth=80bae6ea6e79664dadaa26072b1763acdad87d10",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=0035iQIm2vLSgW&auth=e9057e9aa7e640fdf7c7d7316d6a3d09c40020fe",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=004bAPw11eyb71&auth=550d55a58e33c220cc44af7f10aa5ad7d8a2f221"
      },
      {
        "name": "无渴不湖北",
        "artist": "房东的猫",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=004Zm6Y80JFBhI&auth=1ac1c359a915f2cb25ab6b60390530fe91d46ab2",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=0018XN2S2I2cuS&auth=7b0efddceea8b30f8ab3a16fbe19480044756d71",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=004Zm6Y80JFBhI&auth=77c96c19e1e6f444e445f28dfd933953eb056376"
      },
      {
        "name": "亲爱的你",
        "artist": "严正岚 / 房东的猫",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=0026PBM4273pHe&auth=11c33adcd39a52bbe996082f2969cc00463651d0",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=001e7BlB2vv0DB&auth=5d88610c3b2d10fb749a08405ddac024f9fdd14e",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=0026PBM4273pHe&auth=1d8d756080d0d95e2d72d34c4988f2ab12463ecf"
      },
      {
        "name": "我可以",
        "artist": "房东的猫",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=0002lJFu0mSpGJ&auth=c7f9320e83b1c4a2697108c953d6ea4b73de2115",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=003oCARp0Nlbpy&auth=ad98b452b6d94faa74eef6e0a9c88f7d45a4ecdf",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=0002lJFu0mSpGJ&auth=e22d04a16c2233e84b6760dbffd3d2842c4d4ac7"
      },
      {
        "name": "再见文汇路",
        "artist": "安来宁 / 房东的猫",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=003JgqAt1Zki33&auth=35af432be248af5cd85e29beb5c476c3cf9cb596",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=000Q0Sh21WrCKD&auth=3fa0686d77a1af5e55816df4bcedb0bf78a4fe36",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=003JgqAt1Zki33&auth=ce24e0ec2f50853ebcecb3edf7547be34df3bb9b"
      },
      {
        "name": "那些花儿",
        "artist": "房东的猫 / 谢春花",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=000iOH1k2Onvli&auth=0450b21d1990f0e2f841be6a0f1a2c1321c8505e",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=003sd6L32TqqtR&auth=05c9345c3d81f0c8da7d030ed3d827686057b404",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=000iOH1k2Onvli&auth=a3a21b93bd35c01878fc4bdb79b2f1ec0aa2e23b"
      },
      {
        "name": "美好事物-再遇少年",
        "artist": "边程 / 房东的猫",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=001UQWNm3xGayh&auth=6377b3e5f1e1a7821600bfc9869d8027952ffae9",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=000CmN3V3jH91C&auth=762d0eb0e03485f9c6c618586562098470d6085d",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=001UQWNm3xGayh&auth=3c2741c12b99eec504797a012d5951aae375bc46"
      },
      {
        "name": "蝴蝶之夏",
        "artist": "房东的猫",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=003Jz9i50RuBcV&auth=1dc7cec7af77dd02cef85aebb999f45df6796a80",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=003w1jij2XuU3Q&auth=be751cc049b5a031faf829d1528934d97d0d6b42",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=003Jz9i50RuBcV&auth=d41afbbe3d615eaf244892ba58bd23a71e815137"
      },
      {
        "name": "一百个不喜欢你的方法",
        "artist": "房东的猫",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=004YfAtk3mxPd4&auth=eb0047bff9e8bbe7c748d12b5553efb1ddd33325",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=0048VMT329wFfD&auth=3dc95b970cee5bdcf0b39260758e41d10452f3b5",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=004YfAtk3mxPd4&auth=19e4ee3ee4a59da18ebb1c1a6e09ee2df4c690b5"
      },
      {
        "name": "至此",
        "artist": "房东的猫",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=003lQ4KJ06yGqb&auth=2979337faccfb8a0ef17eda166c48e7b1598e5f7",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=004b0rFW16tU3N&auth=05e4961686153d0b031e8db4cc034880f0a9e5be",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=003lQ4KJ06yGqb&auth=c69ff540e2f3e9a123441156a8147b23bf42a9b6"
      },
      {
        "name": "无渴不湖北",
        "artist": "房东的猫",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=003m4gHR4ZYH0N&auth=d03acc95bc719afcbfecc16b4affbcd16bae429f",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=0018XN2S2I2cuS&auth=7b0efddceea8b30f8ab3a16fbe19480044756d71",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=003m4gHR4ZYH0N&auth=9918d62cb1a02316cc58bb682d9f2f8054f72e8f"
      },
      {
        "name": "时光，一如既往",
        "artist": "房东的猫",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=000kFgnJ4CM5X7&auth=0d4372b4f1b7ccd8a779f910f0851b9ed0279119",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=003VmuMe2FFMRz&auth=c74925f16f68bdfec47346adaf8da027f4da2024",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=000kFgnJ4CM5X7&auth=8839a988ebb17337eff04dc02ba3b109fc849f83"
      },
      {
        "name": "颠倒",
        "artist": "房东的猫",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=000U8CPy2use8p&auth=b3c17d84e88448706ed409ebe3a9cd9218aec0f0",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=001ZaCQY2OxVMg&auth=d0c0f36399d923ff85fd7f93677c1343ef5ce785",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=000U8CPy2use8p&auth=37a1d2acc74d7f6bca84faec2d3ee252762d791a"
      },
      {
        "name": "颠倒",
        "artist": "房东的猫",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=000Tiwqn12RpW0&auth=12f6b6a84008758e72c1b4b665384730ffce7f1f",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=002VrNqW4ROUHz&auth=8c708b7aabf93bd823b3202e4b074f78e59a778f",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=000Tiwqn12RpW0&auth=fba1786fb47184d992adacf3637de8b1846a0bd1"
      },
      {
        "name": "远在咫尺的爱",
        "artist": "房东的猫",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=001QHuR00KIPvr&auth=91e606775a3ecd9b14cd4e6129c946bf13ff72bc",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=000o6A2x0Mo6O4&auth=e6b6a4e6b564714f2d8df7e399bf6df92b910374",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=001QHuR00KIPvr&auth=6205d563097792576e5b001b793c5362f019584a"
      },
      {
        "name": "是初恋是心动是你",
        "artist": "房东的猫",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=002lWSyT1y3VMP&auth=1c4eb43f8fd5d20c3314217012fb00ad21e0ab3e",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=0032wm9x0VO0VG&auth=27e9ca1f37fd0a3a5fda8a73d18e944a356e258c",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=002lWSyT1y3VMP&auth=0c41f181546be7c84f07af69ae7ec437236774ba"
      },
      {
        "name": "初恋的朦胧诗",
        "artist": "房东的猫",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=002BAUh94X2hIS&auth=ea4406c821caa135d20b0e1dcf50d522526e29cb",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=002sw36F1R2PK1&auth=2506b40cf93cafe5e14b05a09b804b407a09bf14",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=002BAUh94X2hIS&auth=02072f7ae995d3769c0aa6ad04fac3032a2e5702"
      },
      {
        "name": "青梅",
        "artist": "房东的猫",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=004KwZ3W1oLefp&auth=877a4ba48bd59199208318e9f43cd0c7c2cc6d22",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=001QIWpQ4FaLuC&auth=4fb55b57e46bb123ec18a2067b38012bcfa068df",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=004KwZ3W1oLefp&auth=762c7938a55153c3f4afeb941474e7f399c6d7e0"
      },
      {
        "name": "锦瑟",
        "artist": "房东的猫",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=001OIniL0Sa13u&auth=015c38a902b6e478f4f9493210037557451b3a16",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=002DzQ632vXrHN&auth=b5a7adb5d472a295ffe18c8d5da3bf6813be258e",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=001OIniL0Sa13u&auth=85887659ef8024b3f34da60129de4ee80cbdcf43"
      },
      {
        "name": "仓颉",
        "artist": "房东的猫",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=001XokYr3VVfHI&auth=a561edde9df146dbca23ca7e4383256e77a02ff3",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=003IFBqi2nE3LO&auth=84db637267c43640d09698038d2d1ada3da4c5dd",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=001XokYr3VVfHI&auth=0a1dcf4776040eb1373429d9a77254f940fcf4ae"
      },
      {
        "name": "见信如晤（第四期聊天电台）",
        "artist": "房东的猫",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=0041YS8w1vJhyK&auth=543db90ab5e17cf9b808f71d177c354e7ad36f65",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=003IFBqi2nE3LO&auth=84db637267c43640d09698038d2d1ada3da4c5dd",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=0041YS8w1vJhyK&auth=015e484d00dc5d5aa47d9babe04a188e4768d0e4"
      },
      {
        "name": "时光，一如既往",
        "artist": "房东的猫",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=0024Xvz40UrLfK&auth=6251a0653cb764641205ee9efc0d9efa90558e4a",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=003IFBqi2nE3LO&auth=84db637267c43640d09698038d2d1ada3da4c5dd",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=0024Xvz40UrLfK&auth=7bef981f09a0a44ee84d15bb065537082114e763"
      },
      {
        "name": "南山南",
        "artist": "房东的猫",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=003Kgk7d3B9XYm&auth=eb019f9f145a95ce6c88bae13754c604c6de1b5d",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=003IFBqi2nE3LO&auth=84db637267c43640d09698038d2d1ada3da4c5dd",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=003Kgk7d3B9XYm&auth=b2a9d357a387b11f4dde27b1f1f4c8574554559c"
      },
      {
        "name": "少年锦时",
        "artist": "房东的猫",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=002g5nEO3XC3ae&auth=4ff88eb32f0dc5375dd2c9b0edab1a8e6633b751",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=003IFBqi2nE3LO&auth=84db637267c43640d09698038d2d1ada3da4c5dd",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=002g5nEO3XC3ae&auth=d0459dc5bef73af72bc6576fe0fdb353806ee712"
      },
      {
        "name": "天台爱情故事 ( 第二期聊天电台 )",
        "artist": "房东的猫",
        "url": "https://api.i-meto.com/meting/api?server=tencent&type=url&id=001ivLcQ4LLanu&auth=d51c3a015d888e53a1ad0bf91f3369610432e6cd",
        "cover": "https://api.i-meto.com/meting/api?server=tencent&type=pic&id=003IFBqi2nE3LO&auth=84db637267c43640d09698038d2d1ada3da4c5dd",
        "lrc": "https://api.i-meto.com/meting/api?server=tencent&type=lrc&id=001ivLcQ4LLanu&auth=345e45199a51a7013bffaecb727650a875e0e675"
      }]
  }
  return new APlayer(options)
}
export {
  musicPlayer
}
