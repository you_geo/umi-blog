interface ProxyStorage {
  readonly storage: Storage,

  getItem(key: string): object | Array<any>,

  setItem(key: string, value: string | object | Array<any>): void,

  removeItem(key: string): void

  clear(): void
}

class ProxyWindowStorage implements ProxyStorage {
  readonly storage: Storage;
  constructor(storage: Storage) {
    this.storage = storage
  }

  clear(): void {
    this.storage.clear()
  }

  getItem(key: string): object | Array<any> {
    return JSON.parse(<string>this.storage.getItem(key))
  }

  removeItem(key: string): void {
    this.storage.removeItem(key)
  }

  setItem(key: string, value: string | object | Array<any>): void {
    this.storage.setItem(key, JSON.stringify(value))
  }

}
const ProxyLocalStorage=new ProxyWindowStorage(localStorage)
const ProxySessionStorage=new ProxyWindowStorage(sessionStorage)
export {
  ProxySessionStorage,
  ProxyLocalStorage
}
