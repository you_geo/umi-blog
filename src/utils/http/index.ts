import {extend, RequestOptionsInit,ResponseError} from 'umi-request'
import { notification } from "antd";
import NProgress from '@/utils/progress'

const codeMessage: { [key: number]: string } = {
  200: "服务器成功返回请求的数据。",
  201: "新建或修改数据成功。",
  202: "一个请求已经进入后台排队（异步任务）。",
  204: "删除数据成功。",
  400: "发出的请求有错误，服务器没有进行新建或修改数据的操作。",
  401: "用户没有权限（令牌、用户名、密码错误）。",
  403: "用户得到授权，但是访问是被禁止的。",
  404: "发出的请求针对的是不存在的记录，服务器没有进行操作。",
  406: "请求的格式不可得。",
  410: "请求的资源被永久删除，且不会再得到的。",
  422: "当创建一个对象时，发生一个验证错误。",
  500: "服务器发生错误，请检查服务器。",
  502: "网关错误。",
  503: "服务不可用，服务器暂时过载或维护。",
  504: "网关超时。",
};
// 请求配置文件
const request = extend({
  // `baseURL` 将自动加在 `url` 前面，除非 `url` 是一个绝对 URL。
  // 它可以通过设置一个 `baseURL` 便于为 axios 实例的方法传递相对 URL
  prefix:'/api',
  timeout: 1000 * 15,
  headers: {
    "Content-Type": " application/json;charset=UTF-8",
  },
  errorHandler(error:ResponseError){
    const {options:{status,method,url}}=error.request
    notification.error({
      message:`${status} ${method} ${url}`,
      description:codeMessage[status]
    })
    NProgress.done()
  }
})
// request interceptor, change url or options.
request.interceptors.request.use((url:string, options:RequestOptionsInit) => {
  NProgress.start()
  return {
    url: url,
    options: { ...options, interceptors: true },
  };
});
// response interceptor, chagne response
request.interceptors.response.use((response:Response, options:RequestOptionsInit) => {
  options.status=response.status
  NProgress.done()
  return response;
});
export {
  request
}

