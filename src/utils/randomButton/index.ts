class RandomButton{
  private randomBtn: HTMLButtonElement|null;
  private flag: number;
  constructor() {
    this.randomBtn=document.querySelector('#randomBtn');
    this.flag=1
    this.randomButton()
  }
  randomButton=()=>{
    this.randomBtn?.addEventListener('mouseover',()=>{
      let handler=new Map([
        [
          1,
          ()=>{
            Object.assign(this.randomBtn?.style,{
              top:0+'px',left:-150+'px'
            })
            this.flag=2;
          }
        ],
        [
          2,
          ()=>{
            Object.assign(this.randomBtn?.style,{
              top:220+'px',left:290+'px'
            })
            this.flag=3;
          }
        ],
        [
          3,
          ()=>{
            Object.assign(this.randomBtn?.style,{
              top:-380+'px',left:-200+'px'
            })
            this.flag=4
          }
        ],
        [
          4,
          ()=>{
            Object.assign(this.randomBtn?.style,{
              top:-380+'px',left:550+'px'
            })
            this.flag=5
          }
        ],
        [
          5,
          ()=>{
            Object.assign(this.randomBtn?.style,{
              top:200+'px',left:630+'px'
            })
            this.flag=1
          }
        ]
      ])
      const fn=handler.get(this.flag)
      fn?.()
    })
    this.randomBtn?.addEventListener('click',()=>{
      alert('咋手抖了啊，小可爱是不是哪里不舒服？')
    })
  }

}

export {
  RandomButton
}
