import $ from 'jquery'

class RENDERER {
  private readonly POINT_INTERVAL: number;
  private readonly FISH_COUNT: number;
  private readonly MAX_INTERVAL_COUNT: number;
  private INIT_HEIGHT_RATE: number;
  private readonly THRESHOLD: number;
  private $window: JQuery<Window>;
  private $container: JQuery<HTMLElement>;
  private readonly context: any;
  private $canvas: JQuery<HTMLCanvasElement>;
  private fishes: any[] = [];
  private readonly points: any[] = [];
  private watchIds: any[] = [];
  private pointInterval: number = 0;
  private width: number = 0;
  private tmpWidth: number = 0;
  private height: number = 0;
  private intervalCount: number = 0;
  private fishCount: number = 0;
  private reverse: boolean = false;
  private axis: { x: number; y: number } = {x: 0, y: 0};
  private tmpHeight: number = 0;
  private WATCH_INTERVAL: number = 0;

  constructor() {
    this.POINT_INTERVAL = 5
    this.FISH_COUNT = 3
    this.MAX_INTERVAL_COUNT = 50
    this.INIT_HEIGHT_RATE = 0.5
    this.THRESHOLD = 50
    this.$window = $(window);
    this.$container = $('#fish-container');
    this.$canvas = $('<canvas />');
    this.context = this.$canvas.appendTo(this.$container).get(0).getContext('2d');
    this.points = [];
    this.fishes = [];
    this.watchIds = [];
    this.init()
  }

  init() {
    this.reconstructMethods();
    this.setup();
    this.bindEvent();
    this.render();
  }


  createSurfacePoints() {
    let count = Math.round(this.width / this.POINT_INTERVAL);
    this.pointInterval = this.width / (count - 1);
    // @ts-ignore
    this.points.push(new SURFACE_POINT(this, 0));

    for (let i = 1; i < count; i++) {
      // @ts-ignore
      let point = new SURFACE_POINT(this, i * this.pointInterval),
        previous = this.points[i - 1];

      point.setPreviousPoint(previous);
      previous.setNextPoint(point);
      this.points.push(point);
    }
  }

  reconstructMethods() {
    this.watchWindowSize = this.watchWindowSize.bind(this);
    this.jdugeToStopResize = this.jdugeToStopResize.bind(this);
    this.startEpicenter = this.startEpicenter.bind(this);
    this.moveEpicenter = this.moveEpicenter.bind(this);
    this.reverseVertical = this.reverseVertical.bind(this);
    this.render = this.render.bind(this);
  }

  setup() {
    this.points.length = 0;
    this.fishes.length = 0;
    this.watchIds.length = 0;
    this.intervalCount = this.MAX_INTERVAL_COUNT;
    this.width = this.$container.width() as number;
    this.height = this.$container.height() as number;
    this.fishCount = this.FISH_COUNT * this.width / 500 * this.height / 500;
    this.$canvas.attr({
      width: this.width,
      height: this.height
    });
    this.reverse = false;
    // @ts-ignore
    this.fishes.push(new FISH(this));
    this.createSurfacePoints();
  }

  watchWindowSize() {
    this.clearTimer();
    this.tmpWidth = this.$window.width() as number;
    this.tmpHeight = this.$window.height() as number;
    this.watchIds.push(setTimeout(this.jdugeToStopResize, this.WATCH_INTERVAL));
  }

  clearTimer() {
    while (this.watchIds.length > 0) {
      clearTimeout(this.watchIds.pop());
    }
  }

  jdugeToStopResize() {
    let width = this.$window.width(),
      height = this.$window.height(),
      stopped = (width == this.tmpWidth && height == this.tmpHeight);

    this.tmpWidth = width as number;
    this.tmpHeight = height as number;

    if (stopped) {
      this.setup();
    }
  }

  bindEvent() {
    this.$window.on('resize', this.watchWindowSize);
    this.$container.on('mouseenter', this.startEpicenter);
    this.$container.on('mousemove', this.moveEpicenter);
    this.$container.on('click', this.reverseVertical);
  }

  getAxis(event: { clientX: number; clientY: number; }) {
    let offset = this.$container.offset() as {left:number,top:number};

    return {
      // @ts-ignore
      x: event.clientX - offset.left + this.$window.scrollLeft(),
      // @ts-ignore
      y: event.clientY - offset.top + this.$window.scrollTop()
    };
  }

  startEpicenter(event: { clientX: number; clientY: number; }) {
    this.axis = this.getAxis(event);
  }

  moveEpicenter(event: { clientX: number; clientY: number; }) {
    let axis = this.getAxis(event);

    if (!this.axis) {
      this.axis = axis;
    }
    this.generateEpicenter(axis.x, axis.y, axis.y - this.axis.y);
    this.axis = axis;
  }

  generateEpicenter(x: number, y: number, velocity: number) {
    if (y < this.height / 2 - this.THRESHOLD || y > this.height / 2 + this.THRESHOLD) {
      return;
    }
    let index = Math.round(x / this.pointInterval);

    if (index < 0 || index >= this.points.length) {
      return;
    }
    this.points[index].interfere(y, velocity);
  }

  reverseVertical() {
    this.reverse = !this.reverse;

    for (let i = 0, count = this.fishes.length; i < count; i++) {
      this.fishes[i].reverseVertical();
    }
  }

  controlStatus() {
    for (let i = 0, count = this.points.length; i < count; i++) {
      this.points[i].updateSelf();
    }
    for (let i = 0, count = this.points.length; i < count; i++) {
      this.points[i].updateNeighbors();
    }
    if (this.fishes.length < this.fishCount) {
      if (--this.intervalCount == 0) {
        this.intervalCount = this.MAX_INTERVAL_COUNT;
        // @ts-ignore
        this.fishes.push(new FISH(this));
      }
    }
  }

  render() {
    requestAnimationFrame(this.render);
    this.controlStatus();
    this.context.clearRect(0, 0, this.width, this.height);
    this.context.fillStyle = '#B3CCE6';

    for (let i = 0, count = this.fishes.length; i < count; i++) {
      this.fishes[i].render(this.context);
    }
    this.context.save();
    this.context.globalCompositeOperation = 'xor';
    this.context.beginPath();
    this.context.moveTo(0, this.reverse ? 0 : this.height);

    for (let i = 0, count = this.points.length; i < count; i++) {
      this.points[i].render(this.context);
    }
    this.context.lineTo(this.width, this.reverse ? 0 : this.height);
    this.context.closePath();
    this.context.fill();
    this.context.restore();
  }
}


class SURFACE_POINT {
  private SPRING_CONSTANT: number = 0.03
  private SPRING_FRICTION: number = 0.9
  private WAVE_SPREAD: number = 0.3
  private ACCELARATION_RATE: number = 0.01
  private renderer: any;
  private readonly x: number = 0;
  private initHeight: number = 0;
  private height: number = 0;
  private force: { next: number; previous: number } = {next: 0, previous: 0};
  private fy: number = 0;
  private previous: any;
  private next: any;

  constructor(renderer: { POINT_INTERVAL: number; FISH_COUNT: number; MAX_INTERVAL_COUNT: number; INIT_HEIGHT_RATE: number; THRESHOLD: number; init: () => void; setParameters: () => void; createSurfacePoints: () => void; reconstructMethods: () => void; setup: () => void; watchWindowSize: () => void; clearTimer: () => void; jdugeToStopResize: () => void; bindEvent: () => void; getAxis: (event: any) => { x: any; y: any; }; startEpicenter: (event: any) => void; moveEpicenter: (event: any) => void; generateEpicenter: (x: any, y: any, velocity: any) => void; reverseVertical: () => void; controlStatus: () => void; render: () => void; }, x: number) {
    this.renderer = renderer;
    this.x = x;
    this.init();
  }

  init() {
    this.initHeight = this.renderer.height * this.renderer.INIT_HEIGHT_RATE;
    this.height = this.initHeight;
    this.fy = 0;
    this.force = {
      previous: 0,
      next: 0
    };
  }

  setPreviousPoint(previous: any) {
    this.previous = previous;
  }

  setNextPoint(next: any) {
    this.next = next;
  }

  interfere(y: number, velocity: number) {
    this.fy = this.renderer.height * this.ACCELARATION_RATE * ((this.renderer.height - this.height - y) >=
    0 ? -1 : 1) * Math.abs(velocity);
  }

  updateSelf() {
    this.fy += this.SPRING_CONSTANT * (this.initHeight - this.height);
    this.fy *= this.SPRING_FRICTION;
    this.height += this.fy;
  }

  updateNeighbors() {
    if (this.previous) {
      this.force.previous = this.WAVE_SPREAD * (this.height - this.previous.height);
    }
    if (this.next) {
      this.force.next = this.WAVE_SPREAD * (this.height - this.next.height);
    }
  }

  render(context: { lineTo: (arg0: number, arg1: number) => void; }) {
    if (this.previous) {
      this.previous.height += this.force.previous;
      this.previous.fy += this.force.previous;
    }
    if (this.next) {
      this.next.height += this.force.next;
      this.next.fy += this.force.next;
    }
    context.lineTo(this.x, this.renderer.height - this.height);
  }
}

class FISH {
  private GRAVITY: number = 0.4
  private renderer: any;
  private direction: boolean = true;
  private x: any;
  private previousY: any;
  private vx: number = 0;
  private y: number = 0;
  private vy: number = 0;
  private ay: number = 0;
  private theta: number = 0;
  private phi: number = 0;
  private isOut: boolean = true;

  constructor(renderer: { POINT_INTERVAL: number; FISH_COUNT: number; MAX_INTERVAL_COUNT: number; INIT_HEIGHT_RATE: number; THRESHOLD: number; init: () => void; setParameters: () => void; createSurfacePoints: () => void; reconstructMethods: () => void; setup: () => void; watchWindowSize: () => void; clearTimer: () => void; jdugeToStopResize: () => void; bindEvent: () => void; getAxis: (event: any) => { x: any; y: any; }; startEpicenter: (event: any) => void; moveEpicenter: (event: any) => void; generateEpicenter: (x: any, y: any, velocity: any) => void; reverseVertical: () => void; controlStatus: () => void; render: () => void; }) {
    this.renderer = renderer;
    this.init();
  };

  init() {
    this.direction = Math.random() < 0.5;
    this.x = this.direction ? (this.renderer.width + this.renderer.THRESHOLD) : -this.renderer.THRESHOLD;
    this.previousY = this.y;
    this.vx = this.getRandomValue(4, 10) * (this.direction ? -1 : 1);

    if (this.renderer.reverse) {
      this.y = this.getRandomValue(this.renderer.height * 1 / 10, this.renderer.height * 4 / 10);
      this.vy = this.getRandomValue(2, 5);
      this.ay = this.getRandomValue(0.05, 0.2);
    } else {
      this.y = this.getRandomValue(this.renderer.height * 6 / 10, this.renderer.height * 9 / 10);
      this.vy = this.getRandomValue(-5, -2);
      this.ay = this.getRandomValue(-0.2, -0.05);
    }
    this.isOut = false;
    this.theta = 0;
    this.phi = 0;
  }

  getRandomValue(min: number, max: number) {
    return min + (max - min) * Math.random();
  }

  reverseVertical() {
    this.isOut = !this.isOut;
    this.ay *= -1;
  }

  controlStatus(context: unknown) {
    this.previousY = this.y;
    this.x += this.vx;
    this.y += this.vy;
    this.vy += this.ay;

    if (this.renderer.reverse) {
      if (this.y > this.renderer.height * this.renderer.INIT_HEIGHT_RATE) {
        this.vy -= this.GRAVITY;
        this.isOut = true;
      } else {
        if (this.isOut) {
          this.ay = this.getRandomValue(0.05, 0.2);
        }
        this.isOut = false;
      }
    } else {
      if (this.y < this.renderer.height * this.renderer.INIT_HEIGHT_RATE) {
        this.vy += this.GRAVITY;
        this.isOut = true;
      } else {
        if (this.isOut) {
          this.ay = this.getRandomValue(-0.2, -0.05);
        }
        this.isOut = false;
      }
    }
    if (!this.isOut) {
      this.theta += Math.PI / 20;
      this.theta %= Math.PI * 2;
      this.phi += Math.PI / 30;
      this.phi %= Math.PI * 2;
    }
    this.renderer.generateEpicenter(this.x + (this.direction ? -1 : 1) * this.renderer.THRESHOLD, this.y,
      this.y - this.previousY);

    if (this.vx > 0 && this.x > this.renderer.width + this.renderer.THRESHOLD || this.vx < 0 && this.x < -
      this.renderer.THRESHOLD) {
      this.init();
    }
  }

  render(context: { save: () => void; translate: (arg0: number, arg1: number) => void; rotate: (arg0: number) => void; scale: (arg0: number, arg1: number) => void; beginPath: () => void; moveTo: (arg0: number, arg1: number) => void; bezierCurveTo: (arg0: number, arg1: number, arg2: number, arg3: number, arg4: number, arg5: number) => void; fill: () => void; quadraticCurveTo: (arg0: number, arg1: number, arg2: number, arg3: number) => void; restore: () => void; closePath: () => void; }) {
    context.save();
    context.translate(this.x, this.y);
    context.rotate(Math.PI + Math.atan2(this.vy, this.vx));
    context.scale(1, this.direction ? 1 : -1);
    context.beginPath();
    context.moveTo(-30, 0);
    context.bezierCurveTo(-20, 15, 15, 10, 40, 0);
    context.bezierCurveTo(15, -10, -20, -15, -30, 0);
    context.fill();

    context.save();
    context.translate(40, 0);
    context.scale(0.9 + 0.2 * Math.sin(this.theta), 1);
    context.beginPath();
    context.moveTo(0, 0);
    context.quadraticCurveTo(5, 10, 20, 8);
    context.quadraticCurveTo(12, 5, 10, 0);
    context.quadraticCurveTo(12, -5, 20, -8);
    context.quadraticCurveTo(5, -10, 0, 0);
    context.fill();
    context.restore();

    context.save();
    context.translate(-3, 0);
    context.rotate((Math.PI / 3 + Math.PI / 10 * Math.sin(this.phi)) * (this.renderer.reverse ? -1 : 1));

    context.beginPath();

    if (this.renderer.reverse) {
      context.moveTo(5, 0);
      context.bezierCurveTo(10, 10, 10, 30, 0, 40);
      context.bezierCurveTo(-12, 25, -8, 10, 0, 0);
    } else {
      context.moveTo(-5, 0);
      context.bezierCurveTo(-10, -10, -10, -30, 0, -40);
      context.bezierCurveTo(12, -25, 8, -10, 0, 0);
    }
    context.closePath();
    context.fill();
    context.restore();
    context.restore();
    this.controlStatus(context);
  }
}

export {
  RENDERER
}
