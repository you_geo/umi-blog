import NProgress from '@/utils/progress'

export function onRouteChange() {
  NProgress.start()
  NProgress.done()
}
