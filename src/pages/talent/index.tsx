import React, {useEffect} from 'react'
import styles from './index.less'
import videoSource from '@/assets/media/fullScreen.mp4'
import {RandomButton} from "utils/randomButton";
import {history} from "umi";

const Talent = () => {
  useEffect(() => {
    new RandomButton()
  })
  return <div className={styles.fullScreen}>
    <video src={videoSource} autoPlay loop muted/>
    <div className={styles.content}>
      <p className={styles.myself}>如果有一天我向你表白，你会怎么样？</p>
      <div className={styles.temp} onClick={(e) => {
        e.preventDefault()
        history.push('/confession')
      }}>
        <a href="" id="agree">
          <span></span>
          <span></span>
          <span></span>
          <span></span>
          你敢表白我就敢答应
        </a>
        <button id='randomBtn' className={styles.randomButton}>残忍拒绝，呜呜呜</button>
      </div>
    </div>
  </div>
}
export default Talent
