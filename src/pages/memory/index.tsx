import React, {useEffect} from 'react'
import './index.less'
import bg1 from '@/assets/image/topimage.jpg'
import cat from '@/assets/image/cat.gif'
import cat2 from '@/assets/image/cat2.gif'
import cat3 from '@/assets/image/cat3.gif'
import cat4 from '@/assets/image/cat4.gif'
import page21 from '@/assets/image/page-2-1.jpg'
import page22 from '@/assets/image/page-2-2.jpg'
import page23 from '@/assets/image/page-2-3.jpg'
import page24 from '@/assets/image/page-2-4.jpg'
import page25 from '@/assets/image/page-2-5.jpg'
import page26 from '@/assets/image/page-2-6.jpg'
import page27 from '@/assets/image/page-2-7.jpg'
import page28 from '@/assets/image/page-2-8.jpg'
import page29 from '@/assets/image/page-2-9.jpg'
import page210 from '@/assets/image/page-2-10.jpg'
import page41 from '@/assets/image/page-4-1.png'
import page42 from '@/assets/image/page-4-2.jpg'
import page43 from '@/assets/image/page-4-3.jpg'
import page44 from '@/assets/image/page-4-4.jpg'
import page45 from '@/assets/image/page-4-5.jpg'
import page46 from '@/assets/image/page-4-6.jpg'
import page47 from '@/assets/image/page-4-7.jpg'
import page48 from '@/assets/image/page-4-8.jpg'
import page49 from '@/assets/image/page-4-9.jpg'
import page410 from '@/assets/image/page-4-10.jpg'
import pig1 from '@/assets/image/pig1.gif'
import pig2 from '@/assets/image/pig2.gif'
import heart from '@/assets/image/001.png'
import {start} from "utils/memory/su";
import TypeIt, {TypeItOptions} from "typeit-react";
import {initSwiper} from 'utils/memory/swiper'

const Memory = () => {
  useEffect(() => {
    start()
    initSwiper()
  }, [])
  return <>
    {/*标题*/}
    <div className="title">
      <img src={bg1} alt="bgGif"/>
      <TypeIt
        // @ts-ignore
        className="tqyxhj"
        options={{
          loop: true,
          cursorSpeed: 1000,
          speed: 100
        } as TypeItOptions}
        getBeforeInit={(instance) => {
          instance.type("初次见面")
            .pause(2000)
            .delete(null, {
              delay: 500
            })
            .type('你好！我叫翼小灵')
            .pause(2000)
            .delete(null, {
              delay: 500
            })
            .type("希望大学四年")
            .pause(2000)
            .delete(null, {
              delay: 500
            })
            .type('<strong class="type-color">❤</strong>有你相伴<strong class="type-color">❤</strong>')
            .pause(3000)
            .go()
          return instance
        }}
      />
    </div>

    {/*分页一*/}
    <div className="page page1">
      <div className="swiper-container swiper-container-lovexhj1">
        <div className="swiper-wrapper">
          <div className="swiper-slide">
            <div className="page1-lovexhj1">
              <img
                src={heart}
                alt="xhj"
              />
              <img src={cat} alt="cat"/>
              <img
                src={heart}
                alt="tqy"
              />
              <p id="lovetime"></p>
              <p>我也不知道该准备一点什么给你逗你开心</p>
              <p>还是写一点好玩的送给你吧~</p>
              <span className="right">往右划动 →</span>
            </div>
          </div>
          <div className="swiper-slide">
            <div className="page1-lovexhj1">
              <img className="cat" src={cat2} alt="cat2"/>
              <p className="catText">你常对我发的表情 ↑</p>
              <p>上次也是花心思写了一个小玩意给你</p>
              <p>但我觉得整体设计&&界面都过于<b>LOW</b></p>
              <p>那就重新送你一个吧</p>
              <span className="right">再划一下 →</span>
            </div>
          </div>
          <div className="swiper-slide">
            <div className="page1-lovexhj1">
              <img className="cat" src={cat3} alt="cat3"/>
              <p className="catText">抱死你 ↑</p>
              <p>这次我<b>超级超级超级超级</b></p>
              <p>认真的写了一遍</p>
              <p style={{fontSize: '13px'}}>
                以 框架构造、
                <span style={{fontSize: '16px', color: 'red'}}>爱你</span>、
                视觉体验、响应式 为核心制作
              </p>
              <span className="right">再划一次吧 →</span>
            </div>
          </div>
          <div className="swiper-slide">
            <div className="page1-lovexhj1">
              <img className="cat" src={cat4} alt="cat4"/>
              <p className="catText">拍你猪头 ↑</p>
              <p>这次应该不会太难看了吧</p>
              <p>希望你喜欢呀</p>
              <p>爱你哟(๑′ᴗ‵๑)</p>
              <b>520快乐我的<span style={{color: 'pink'}}>猪</span></b>
            </div>
          </div>
        </div>
        <div className="swiper-pagination swiper-pagination-lovexhj1"></div>
      </div>
    </div>

    {/*分页二*/}
    <div className="page page2">
      <div className="swiper-container swiper-container-lovexhj2">
        <div className="swiper-wrapper">
          <div className="swiper-slide">
            <div className="page2-box">
              <img src={page21} alt="page2-1"/>
              <p><b>故事的开始</b></p>
              <p>2018-11-19</p>
              <p>我们的故事，从此开始</p>
            </div>
          </div>
          <div className="swiper-slide">
            <div className="page2-box">
              <img src={page22} alt="page2-2"/>
              <p><b>第一次抱我大腿</b></p>
              <p>还记得第一次你走不动的时候抱着我大腿不愿意走的时候吗？</p>
            </div>
          </div>
          <div className="swiper-slide">
            <div className="page2-box">
              <img src={page23} alt="page2-3"/>
              <p><b>第一次喝奶茶</b></p>
              <p>你总是把腿搭在我的大腿上</p>
              <p>扯你帽子的时候我们开心的和小孩一样</p>
            </div>
          </div>
          <div className="swiper-slide">
            <div className="page2-box">
              <img src={page24} alt="page2-4"/>
              <p><b>第一次走在雪地上</b></p>
              <p>天气很冷，但与你在一起玩却很暖和</p>
              <p>一起在雪地上涂鸦，看你画猪头哈哈</p>
            </div>
          </div>
          <div className="swiper-slide">
            <div className="page2-box">
              <img src={page25} alt="page2-5"/>
              <p><b>第一次庆祝你成年生日</b></p>
              <p>刚在一起不久就赶上了你的成年生日</p>
              <p>送你礼物的那一刻你笑的是多么的开心</p>
            </div>
          </div>
          <div className="swiper-slide">
            <div className="page2-box">
              <img src={page26} alt="page2-6"/>
              <p><b>第一次庆祝我的生日</b></p>
              <p>吃牛排看电影</p>
              <p>很俗气但很开心</p>
            </div>
          </div>
          <div className="swiper-slide">
            <div className="page2-box">
              <img src={page27} alt="page2-7"/>
              <p><b>第一次与你看电影</b></p>
              <p>这应该算是第一次认真的看完了一部电影吧哈哈</p>
            </div>
          </div>
          <div className="swiper-slide">
            <div className="page2-box">
              <img src={page28} alt="page2-8"/>
              <p><b>第一次在深夜跑出来陪你</b></p>
              <p>偶然条件下，凌晨三点我们在一起散步</p>
              <p>安静的马路上充满了甜甜的感觉</p>
            </div>
          </div>
          <div className="swiper-slide">
            <div className="page2-box">
              <img src={page29} alt="page2-9"/>
              <p><b>第一次与你打游戏</b></p>
              <p>看你输了“赛车”时生气的样子好好笑</p>
              <p>并偷偷在后面摸了一下你的猪头</p>
            </div>
          </div>
          <div className="swiper-slide">
            <div className="page2-box">
              <img src={page210} alt="page2-10"/>
              <p><b>More？</b></p>
              <p>篇幅有限，</p>
              <p>丢一张丑照吧hahahahahah</p>
            </div>
          </div>
        </div>
        <div className="swiper-pagination swiper-pagination-lovexhj2"></div>
      </div>
    </div>

    {/*分页三*/}
    <div className="page page3">
      <div className="talkToXHJ">
        <TypeIt
          options={{
            lifeLike: true,
            cursorSpeed: 1000,
            waitUntilVisible: true,
            speed: 100,
            loop: true
          } as TypeItOptions}
        >
          <b>回顾上一个520</b>
          <br/>
          当时你从外面特地跑过来找我，我知道你超级超级害怕晕车，但是还是为了当天见到我，坐车回来了，当我听见你要回来的时候我还以为你在骗我哈哈，结果晚上真的见到你了，你一下车就冲过来抱住了我，这个画面回想起来仿佛就发生在昨天一样清晰...
          <br/>
          <br/>
          <b>这是我们的第二个520</b>
          <br/>
          比较直男的我，还是用属于我的方式来送你一个小小的虚拟礼物（上次那个太不用心了哈哈）
          <br/>
          这段时间经常告诉你我在写代码不能陪你玩，其实我都在花心思做这个呢，我想把它做到很好很好，让你感受一下“代码”带来了趣味性
          <br/>
          用心才能做得更好，陪你也是这样，每天的事情大同小异，除去日常琐事剩下的就是陪你，日复一日但是每天都会是不同的感觉，带来不同的乐趣
          <br/>
          陪你玩游戏，陪你聊天，一起互怼一起疯，看似再平常不过的事情在我们这里都充满了开心
          <br/>
          <br/>
          <b>我想</b>
          <br/>
          我们还会有好多好多的520，不过每一次我都要尝试用新鲜的思路去给你带来一些值得回忆的瞬间还是很有挑战性的，对我来说给你准备惊喜的过程总是充满了动力
          <br/>
          用文字来对你说一些话感觉好像情书一样尴尬哈哈，那就不说了我
          <br/>
          节日快乐猪
          <br/>
          ## 爱你的仔仔 (๑′ᴗ‵๑)~ ❤
        </TypeIt>
      </div>


    </div>

    {/*分页四*/}
    <div className="page page4">
      <div className="page4-box">
        <div className="page4-img">
          <div className="swiper-container swiper-container-lovexhj3">
            <div className="swiper-wrapper">
              <div className="swiper-slide">
                <img src={page41} alt="page-4-1"/>
              </div>
              <div className="swiper-slide">
                <img src={page42} alt="page-4-2"/>
              </div>
              <div className="swiper-slide">
                <img src={page43} alt="page-4-3"/>
              </div>
              <div className="swiper-slide">
                <img src={page44} alt="page-4-4"/>
              </div>
              <div className="swiper-slide">
                <img src={page45} alt="page-4-5"/>
              </div>
              <div className="swiper-slide">
                <img src={page46} alt="page-4-6"/>
              </div>
              <div className="swiper-slide">
                <img src={page47} alt="page-4-7"/>
              </div>
              <div className="swiper-slide">
                <img src={page48} alt="page-4-8"/>
              </div>
              <div className="swiper-slide">
                <img src={page49} alt="page-4-9"/>
              </div>
              <div className="swiper-slide">
                <img src={page410} alt="page-4-10"/>
              </div>
            </div>
            <div className="swiper-button-prev swiper-button-prev-lovexhj3"></div>
            <div className="swiper-button-next swiper-button-next-lovexhj3"></div>
          </div>
        </div>
        <div className="page4-text">
          <h1>Send To Future</h1>
          <p>有你在的每一天都如糖果一样甜</p>
        </div>
      </div>


    </div>

    {/*猪猪*/}
    <div className="xhjIsPig">
      <img src={pig1} alt="pig1" className="pig1"/>
      <img src={pig2} alt="pig2" className="pig2"/>
    </div>
  </>
}
export default Memory
