import React from 'react'
import styles from './index.less'
import {Card, Col, Row, Timeline} from 'antd';
import {ClockCircleOutlined} from '@ant-design/icons';


const HistoryList = () => {
  const recordList = [
    ['入坑web前端 2020-03-01', '习得前端三剑客 2020-05-25', 'Vue初尝试 2020-06-01', 'Vue移动端哔哩哔哩开发 2020-08-11'],
    ['nodejs从入门到再次入门 2020-09-01', 'MongoDB+Mysql从删库到跑路 2020-09-25', 'webpack从熟悉到陌生 2020-10-11', 'Vue外包项目，赚的第一桶金 2020-11-07'],
    ['团队考试系统 2020-12-02', 'React初尝试 2021-01-20', 'Vue3+TypeScript 2020-02-25', '团队打卡系统 2020-03-15'],
    ['Vue源码从看懂到看开 2021-4-18', 'umijs从入门到跑路 2021-05-12', '闭环控制管理系统 2020-07-01', 'umi个人博客 2021-09-01']
  ]
  return <div className={styles.recordContainer}>
    <Row justify='center'>
      <Col span={18}>
        <Card>
          {
            recordList.map(arr =>
              <Timeline mode='alternate'>
                <Timeline.Item>{arr[0]}</Timeline.Item>
                <Timeline.Item color="green">{arr[1]}</Timeline.Item>
                <Timeline.Item dot={<ClockCircleOutlined style={{fontSize: '16px'}}/>}>
                  {arr[2]}
                </Timeline.Item>
                <Timeline.Item color="red">{arr[3]}</Timeline.Item>
              </Timeline>
            )
          }
        </Card>
      </Col>
    </Row>
  </div>
}
export default HistoryList
