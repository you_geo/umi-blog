import React, {useState} from 'react'
import {useParams} from 'umi'
import {List, Card, Row, Col} from 'antd'
import styles from './index.less'
import ReactMarkdown from "react-markdown";
import {CalendarOutlined, FireOutlined, FolderOutlined} from "@ant-design/icons";

const Foo = (props: any) => {
  console.log(useParams())

  interface listType {
    title: string,
    desc: string
  }

  let arr: Array<listType> = []
  for (let i = 0; i < 5; i++) {
    arr.push({
      title: 'grid',
      desc: `File类的概述:
    做为Java新手，博主理解其功能大致为对文件或文件夹的操作
    多线程编程优点：
1.进程之间不能共享内存，但线程之间共享内存非常容易。
2.系统创建进程时需要为该进程重新分配系统资源，但创建线程则代价小得多，因此使用多线程来实现多任务并发比多进程的效率高。
3.Java 语言内置了多线程功能支持，而不是单纯地作为底层操作系统的调度方式，从而简化了 Java 的多线程编程。
`
    })
  }
  let markdown: string = `# A demo of \`react-markdown\`

\`react-markdown\` is a markdown component for React.

👉 Changes are re-rendered as you type.

👈 Try writing some markdown on the left.

## Overview

* Follows [CommonMark](https://commonmark.org)
* Optionally follows [GitHub Flavored Markdown](https://github.github.com/gfm/)
* Renders actual React elements instead of using \`dangerouslySetInnerHTML\`
* Lets you define your own components (to render \`MyHeading\` instead of \`h1\`)
* Has a lot of plugins

## Table of contents

Here is an example of a plugin in action
([\`remark-toc\`](https://github.com/remarkjs/remark-toc)).
This section is replaced by an actual table of contents.

## Syntax highlighting

Here is an example of a plugin to highlight [\`rehype-highlight\`](https://github.com/rehypejs/rehype-highlight).

\`\`\`js
import React from 'react'
import ReactDOM from 'react-dom'
import ReactMarkdown from 'react-markdown'
import rehypeHighlight from 'rehype-highlight'

ReactDOM.render(
  <ReactMarkdown rehypePlugins={[rehypeHighlight]}>{'# Your markdown here'}</ReactMarkdown>,
  document.querySelector('#content')
)
\`\`\`

Pretty neat, eh?

## GitHub flavored markdown (GFM)

For GFM, you can *also* use a [\`remark-gfm\`](https://github.com/remarkjs/react-markdown#use).
It adds support for GitHub-specific extensions to the tables, strikethrough, tasklists, and literal URLs.

These features **do not work by default**.
👆 Use the toggle above to add the plugin.

| Feature    | Support              |
| ---------: | :------------------- |
| CommonMark | 100%                 |
| GFM        | 100% w/ \`remark-gfm\` |

~~strikethrough~~

* [ ] task list
* [x] checked item

## HTML in markdown

⚠️ HTML in markdown is quite unsafe, but if you want to support it, you can
use [\`rehype-raw\`](https://github.com/rehypejs/rehype-raw).
You should probably combine it with
[\`rehype-sanitize\`](https://github.com/rehypejs/rehype-sanitize).

<blockquote>
  👆 Use the toggle above to add the plugin.
</blockquote>

## Components

You can pass components to change \`\`\`js
import React from 'react'
import ReactDOM from 'react-dom'
import ReactMarkdown from 'react-markdown'
import MyFancyRule from './components/my-fancy-rule.js'

ReactDOM.render(
  <ReactMarkdown
    components={{
      // Use h2s instead of h1s
      h1: 'h2',
      // Use a component instead of hrs
      hr: ({node, ...props}) => <MyFancyRule {...props} />
    }}
  >
    # Your markdown here
  </ReactMarkdown>,
  document.querySelector('#content')
)
\`\`\`

## More info?

Much more info is available in the
[readme on GitHub](https://github.com/remarkjs/react-markdown)!

***

A component by [Espen Hovlandsdal](https://espen.codes/)`
  const [listData, setListData] = useState<Array<listType>>(arr)
  return <div className={styles.detailContainer}>
    <Row justify='center' gutter={[16, 0]}>
      <Col span={16}>
        <List
          className='box-shadow'
          grid={{gutter: 16}}
          bordered
          header={<>
            <div className={styles.detailTitle}>umijs从入门到跑路</div>
            <div className={styles.detailHeader}>
          <span className={styles.detailIcon}>
              <i style={{color: '#7FD5B9'}}><CalendarOutlined/></i>
              <span>2021-9-1</span>
          </span>
              <span className={styles.detailIcon}>
              <i style={{color: '#F3AA68'}}><FolderOutlined/></i>
              <span>教程</span>
          </span>
              <span className={styles.detailIcon}>
              <i style={{color: '#FE0D0A'}}> <FireOutlined/></i>
              <span>999人</span>
          </span>
            </div>
          </>}
          itemLayout='vertical'
          dataSource={listData}
          renderItem={item => (
            <List.Item
            >
              {/*<Card*/}
              {/*  hoverable*/}
              {/*  bordered*/}
              {/*  bodyStyle={{width:'100%'}}*/}
              {/*>*/}
              <ReactMarkdown
                className={styles.listContent}
                children={markdown}
              />
              {/*</Card>*/}

            </List.Item>
          )
          }
        />
      </Col>
      <Col span={4}>
        <Card
          className='box-shadow'
          cover={<img src='https://img1.baidu.com/it/u=3448025111,2634675126&fm=26&fmt=auto&gp=0.jpg'/>}
        >
          <Card.Meta title='勇敢牛牛，不怕困难' description='冲就完了'/>
        </Card>
      </Col>
    </Row>
  </div>
}
export default Foo
