import React from 'react'
import {Button, Result} from 'antd'

interface historyType extends History {
  push(url: string): void
}

const ErrorPage = (props: { history: historyType }) => {
  return <Result
    status='404'
    title={'404'}
    subTitle={'Sorry, the page you visited does not exist.'}
    extra={
      <Button type="primary" onClick={() => {
        props.history.push('/')
      }}>
        Go Home
      </Button>
    }
  />
}
export default ErrorPage

