import React, {RefObject, useRef, useState} from 'react'
import './index.less'
import {message} from 'antd'

const Login = () => {
  interface signInType {
    email: string | number,
    password: string | number
  }

  interface signUpType extends signInType {
    username: string | number,
  }

  const [signUpData, setSignUpData] = useState<signUpType>({
    username: '',
    email: '',
    password: ''
  })
  const [signInData, setSignInData] = useState<signInType>({
    email: '',
    password: ''
  })
  const container = useRef<RefObject<HTMLDivElement> | any>()
  const signInHandler = () => {
    container.current.classList.remove("right-panel-active");
  }
  const signUpHandler = () => {
    container.current.classList.add("right-panel-active");
  }

  const handlerSignUpSubmit = async (e: any) => {
    e.preventDefault();
    const {username, password, email} = signUpData
    if (!username || !password || !email) {
      await message.error('请将表单填写完整！')
    }
  }

  const handlerSignInSubmit = async (e: any) => {
    e.preventDefault();
    const {password, email} = signInData
    if (!password || !email) {
      await message.error('请将表单填写完整！')
    }
  }
  const collectSignUp = (e: any) => {
    setSignUpData({
      ...signUpData,
      [e.target.name]: e.target.value
    })
  }
  const collectSignIn = (e: any) => {
    setSignInData({
      ...signInData,
      [e.target.name]: e.target.value
    })
  }
  return <div className="login-container">
    <div ref={container} className="container right-panel-active">
      {/*注册*/}
      <div className="container_form container--signup">
        <form onChange={collectSignUp} action="#" className="form" id="form1">
          <h2 className="form_title">Sign Up</h2>
          <input type="text" name='username' placeholder="User" className="input"/>
          <input type="email" name='email' placeholder="Email" className="input"/>
          <input type="password" name='password' placeholder="Password" className="input"/>
          <button onClick={handlerSignUpSubmit} className="btn">Sign Up</button>
        </form>
        {/*<div className="form">*/}
        {/*<Form*/}
        {/*  onFinish={handlerSignUpSubmit}*/}
        {/*>*/}
        {/*  <h2 className="form_title">Sign Up</h2>*/}
        {/*  <Form.Item*/}
        {/*    name='userName'*/}
        {/*    rules={[*/}
        {/*      {*/}
        {/*        required: true,*/}
        {/*        message: '请输入账号'*/}
        {/*      },*/}
        {/*      {*/}
        {/*        min: 3,*/}
        {/*        max: 18,*/}
        {/*        message: '输入账号长度非法，请输入长度在3-18之间的账号'*/}
        {/*      }*/}
        {/*    ]}*/}
        {/*    hasFeedback*/}
        {/*  >*/}
        {/*    <Input placeholder='请输入账号' prefix={<UserOutlined/>}/>*/}
        {/*  </Form.Item>*/}
        {/*  <Form.Item*/}
        {/*    name='passWord'*/}
        {/*    rules={[*/}
        {/*      {*/}
        {/*        required: true,*/}
        {/*        message: '请输入密码'*/}
        {/*      },*/}
        {/*      {*/}
        {/*        min: 3,*/}
        {/*        max: 18,*/}
        {/*        message: '输入密码长度非法，请输入长度在3-18之间的密码'*/}
        {/*      }*/}
        {/*    ]}*/}
        {/*    hasFeedback*/}
        {/*  >*/}
        {/*    <Input.Password placeholder='请输入密码' prefix={<LockOutlined/>}/>*/}
        {/*  </Form.Item>*/}
        {/*  <Form.Item>*/}
        {/*    <Button type='primary' icon={<AliwangwangOutlined/>} shape='round' htmlType='submit'>Sign Up</Button>*/}
        {/*  </Form.Item>*/}
        {/*</Form>*/}
        {/*</div>*/}
      </div>

      {/*登录*/}
      <div className="container_form container--signin">
        <form onChange={collectSignIn} action="#" className="form" id="form2">
          <h2 className="form_title">Sign In</h2>
          <input type="email" name="email" placeholder="Email" className="input"/>
          <input type="password" name='password' placeholder="Password" className="input"/>
          <a href="#" className="link">Forgot your password?</a>
          <button onClick={handlerSignInSubmit} className="btn">Sign In</button>
        </form>
      </div>

      {/*浮层*/}
      <div className="container_overlay">
        <div className="overlay">
          <div className="overlay_panel overlay--left">
            <button onClick={signInHandler} className="btn" id="signIn">Sign In</button>
          </div>
          <div className="overlay_panel overlay--right">
            <button onClick={signUpHandler} className="btn" id="signUp">Sign Up</button>
          </div>
        </div>
      </div>
    </div>

    {/*背景*/
    }
    {/*<div className="slidershow">*/
    }
    {/*  <div className="slidershow--image" style="background-image: url('https://source.unsplash.com/Snqdjm71Y5s')"></div>*/
    }
    {/*  <div className="slidershow--image" style="background-image: url('https://source.unsplash.com/5APj-fzKE-k')"></div>*/
    }
    {/*  <div className="slidershow--image" style="background-image: url('https://source.unsplash.com/wnbBH_CGOYQ')"></div>*/
    }
    {/*  <div className="slidershow--image" style="background-image: url('https://source.unsplash.com/OkTfw7fXLPk')"></div>*/
    }
    {/*</div>*/
    }
  </div>
}
export default Login
