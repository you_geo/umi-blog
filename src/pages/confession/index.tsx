import React from 'react'
import backgroundImage from '@/assets/image/bg.png'
import styles from './index.less'
const Confession=()=>{
  return <div className={styles.container}>
    <div className={styles.heart}>
      <div className={styles.rightEye}/>
      <span/>
      <div className={styles.leftEye}/>
      <div className={styles.mouth}/>
    </div>
    <img className={styles.heartImage} src={backgroundImage} alt=""/>
    <p>做我npy吧，行的话就答应，不行的话我再想想办法QAQ</p>
  </div>
}
export default Confession
