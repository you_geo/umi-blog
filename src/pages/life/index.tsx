import React, {useEffect, useState} from 'react'
import {Card, Col, List, Row, Carousel} from 'antd'
import styles from './index.less'
import preIcon from '@/assets/swiper/preImg.png'
import nextIcon from '@/assets/swiper/nexImg.png'
import {Swiper} from "utils/swiper";

const Life = () => {

  useEffect(() => {
    new Swiper('#swiper')
  })
  const [imgList, setImgList] = useState<Array<imgListTye>>([{
    src: 'https://img1.baidu.com/it/u=1400627015,3512878464&fm=26&fmt=auto&gp=0.jpg',
    id: '1400627015'
  }, {
    src: 'https://img1.baidu.com/it/u=482823002,2048659319&fm=26&fmt=auto&gp=0.jpg',
    id: '482823002'
  }, {
    src: 'https://img1.baidu.com/it/u=3374271203,2731218561&fm=26&fmt=auto&gp=0.jpg',
    id: '108036953469981'
  }, {
    src: 'https://img1.baidu.com/it/u=3314252932,127218969&fm=26&fmt=auto&gp=0.jpg',
    id: '108036953'
  }, {
    src: 'https://img1.baidu.com/it/u=715541450,697373288&fm=26&fmt=auto&gp=0.jpg',
    id: '140062'
  }, {
    src: 'https://img1.baidu.com/it/u=2673390958,3377558716&fm=26&fmt=auto&gp=0.jpg',
    id: '2233'
  }])
  return <Row className={styles.lifeContainer} justify='center'>
    <Col span={18}>
      {/*<Carousel*/}
      {/*  autoplay*/}
      {/*  className={styles.carousel}*/}
      {/*>*/}
      {/*  {*/}
      {/*    imgList.map(item => <img className={styles.image} src={item.src} alt=''/>)*/}
      {/*  }*/}
      {/*</Carousel>*/}
      <ul className={styles.swiperContainer} id='swiper'>
        {
          imgList.map(item => (<li key={item.id}>
            <img src={item.src} alt=""/>
          </li>))
        }
        <img src={preIcon} id='preIcon' className={styles.preIcon} alt=""/>
        <img src={nextIcon} id='nextIcon' className={styles.nextIcon} alt=""/>
        <div id='swiperFooter' className={styles.bottomCircleContainer}>
          {
            imgList.map(item => (
              <span key={item.id} className={styles.bottomCircle}/>
            ))
          }
        </div>
      </ul>
      <List
        className={`box-shadow ${styles.listContainer}`}
        grid={{
          gutter: 16,
          column: 3
        }}
        dataSource={imgList}
        renderItem={(item) => (

          <List.Item>
            <Card
              hoverable
              cover={<img
                src={item.src} alt=''/>}
            >
              <Card.Meta title='animation' description='好耶'/>
            </Card>
          </List.Item>

        )}
      >
      </List>
    </Col>
  </Row>
}
export default Life
