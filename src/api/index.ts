import {request} from "@/utils/http";
import {ResponseType} from 'umi-request'
const getBlogList=<T=Array<object>>(data?:T):Promise<ResponseType> =>request.get('/getBlogList')
export {
  getBlogList
}
