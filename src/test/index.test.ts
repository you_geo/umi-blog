import {demo,promise} from './demo'

describe('父级分组',()=>{
  beforeAll(()=>{
    console.log('开始测试')
  })
  afterAll(()=>{
    console.log('完成测试')
  })
  beforeEach(()=>{
    console.log('start')
  })
  afterEach(()=>{
    console.log('end')
  })
  describe('group1',()=>{
    beforeEach(()=>{
      console.log('group1 start')
    })
    afterEach(()=>{
      console.log('group1 end')
    })
    test('值匹配',()=>{
      expect(demo(323)).toEqual({name:'cdd'})
    })
  })
  describe('group2',()=>{
    beforeEach(()=>{
      console.log('group2 start')
    })
    afterEach(()=>{
      console.log('group2 end')
    })
    test('async',(done)=>{
      promise().then(res=>{
        expect(res).toBe(2233)
        done()
      })
    })
    test('sync',async ()=>{
      await expect(promise()).resolves.toBe(2233)
    })
  })

})


