import React, {
  ReactElement,
  CSSProperties,
  useEffect,
  PropsWithChildren
} from 'react';
import Header from './header'
import {BackTop, notification} from 'antd'
import {CaretUpOutlined, SmileOutlined} from '@ant-design/icons'
import styles from './index.less'
import {RENDERER} from "@/utils/fish";
import {clickHandler} from "@/utils/click";
import ReactHowler from 'react-howler'
import audioSource from '@/assets/media/bg.mp3'
import '@/utils/flower'
import {musicPlayer} from "@/utils/player";
import {IRouteProps, withRouter} from 'umi'
import {TransitionGroup, CSSTransition} from 'react-transition-group';
// 根据前进还是后退显示不同的转场动画效果
const ANIMATION_MAP: Record<string, string> = {
  PUSH: 'forward',
  POP: 'back'
};
const Layouts = withRouter((props: PropsWithChildren<IRouteProps>) => {
    useEffect(() => {
      new RENDERER()
      new clickHandler()
      musicPlayer('#aPlayer')
      notification.open({
        message: '欢迎来到我的博客',
        description: <div><strong className="type-color">❤</strong>海边微风起，等风也等你<strong className="type-color">❤</strong>
        </div>,
        icon: <SmileOutlined style={{color: '#108ee9'}}/>,
      })
    }, [])
    const style: CSSProperties = {
      height: 40,
      width: 40,
      lineHeight: '40px',
      borderRadius: 40,
      backgroundColor: '#1088e9',
      color: '#fff',
      textAlign: 'center',
      fontSize: 26,
    };
    return (
      <>
        <Header/>
        <div className={styles.fishContainer} id='fish-container'/>
        <div className={styles.clickContainer} id="clickCanvas"/>
        <BackTop>
          <div style={style}><CaretUpOutlined/></div>
        </BackTop>
        {/*<ReactHowler*/}
        {/* src={audioSource}*/}
        {/* playing={true}*/}
        {/* loop={true}*/}
        {/*/>*/}
        <div id='aPlayer' className={styles.playerContainer}/>

        {/*这个组件的详细应用 见官网 这里就直接写现成的了*/}
        <TransitionGroup
          childFactory={child => React.cloneElement(child, {classNames: ANIMATION_MAP[props.history.action]})}>
          <CSSTransition key={props.location.pathname} timeout={300}>
            <div className={styles.layoutContainer}>
              {props.children}
            </div>
          </CSSTransition>
        </TransitionGroup>
      </>
    )
  }
)
export default Layouts
