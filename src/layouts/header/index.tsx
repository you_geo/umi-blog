import React from 'react'
import {Link, withRouter} from 'umi'
import styles from './index.less'
import {Col, Menu, Row} from 'antd'
import {CalendarOutlined, HeartOutlined, HomeOutlined, SmileOutlined, UserOutlined} from '@ant-design/icons'

const Header = withRouter(({location}) => {
  return <div className={`${styles.headerContainer}`}>
    <Row justify='space-around'>
      <Col span={6}>
        <span className={styles.headerName}>眷思量</span>
        <span className={styles.headerDesc}>人间烟火味，最抚凡人心</span>
      </Col>
      <Col span={12}>
        <Menu
          mode='horizontal'
          defaultSelectedKeys={[location.pathname.substring(1) || '']}
        >
          <Menu.Item key='' icon={<HomeOutlined/>}>
            <Link to='/'>首页</Link>
          </Menu.Item>
          <Menu.Item key='record' icon={<CalendarOutlined/>}>
            <Link to='/record'>记录</Link>
          </Menu.Item>
          <Menu.Item key='talent' icon={<HeartOutlined/>}>
            <Link to='/talent'>才艺</Link>
          </Menu.Item>
          <Menu.Item key='life' icon={<SmileOutlined/>}>
            <Link to='/life'>生活</Link>
          </Menu.Item>
          <Menu.Item key='memory' icon={<HeartOutlined/>}>
            <Link to='/memory'>记忆</Link>
          </Menu.Item>
          <Menu.Item key='login' icon={<UserOutlined/>}>
            <Link to='/login'>登录</Link>
          </Menu.Item>
        </Menu>
      </Col>
    </Row>
  </div>
})
export default Header
