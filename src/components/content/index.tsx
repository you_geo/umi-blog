import React, {useEffect} from 'react'
import {Col, Row} from 'antd'
import styles from './index.less'
import ListComponent from "./list";
import PersonalInfo from "./personalInfo";

const Content = () => {
  useEffect(() => {
    // getBlogList().then(r => console.log(r))
  }, [])
  return <div className={styles.contentContainer}>
    <Row justify='center' gutter={[20, 0]}>
      <Col span={16}>
        <ListComponent/>
      </Col>
      <Col span={4}>
        <PersonalInfo/>
      </Col>
    </Row>
  </div>
}
export default Content
