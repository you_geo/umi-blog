import React, {useState} from 'react'
import {history} from 'umi'
import styles from './index.less'
import {Card, List} from "antd";
import {CalendarOutlined, FireOutlined, FolderOutlined} from "@ant-design/icons";

const ListComponent = () => {
  interface listType {
    title: string,
    desc: string
  }

  let arr: Array<listType> = []
  for (let i = 0; i < 10; i++) {
    arr.push({
      title: 'Umi 是什么？',
      desc: `Umi，中文可发音为乌米，是可扩展的企业级前端应用框架。Umi 以路由为基础的，同时支持配置式路由和约定式路由，保证路由的功能完备，并以此进行功能扩展。然后配以生命周期完善的插件体系，覆盖从源码到构建产物的每个生命周期，支持各种功能扩展和业务需求。

Umi 是蚂蚁金服的底层前端框架，已直接或间接地服务了 3000+ 应用，包括 java、node、H5 无线、离线（Hybrid）应用、纯前端 assets 应用、CMS 应用等。他已经很好地服务了我们的内部用户，同时希望他也能服务好外部用户。
`
    })
  }
  const [listData, setListData] = useState<Array<listType>>(arr)

  const navigatorToDetail = () => {
    history.push(`/detail/${23}`)
    window.scrollTo(0, 0)
  }

  return <List
    className='box-shadow'
    grid={{gutter: 16}}
    header={<div className={styles.cardTitle}>首页</div>}
    itemLayout='vertical'
    dataSource={listData}
    renderItem={item => (
      <List.Item>
        <Card
          hoverable
          bodyStyle={{display: 'flex', height: '280px'}}
          bordered
          onClick={navigatorToDetail}
        >
          <div className={styles.cardLeft}>
            <div className={styles.listTitle}>{item.title}</div>
            <div className={styles.listHeader}>
              <span className={styles.listIcon}>
                    <i style={{color: '#7FD5B9'}}><CalendarOutlined/></i>
                    <span>2021-9-1</span>
              </span>
              <span className={styles.listIcon}>
                    <i style={{color: '#F3AA68'}}><FolderOutlined/></i>
                    <span>教程</span>
              </span>
              <span className={styles.listIcon}>
                    <i style={{color: '#FE0D0A'}}> <FireOutlined/></i>
                    <span>999人</span>
              </span>
            </div>
            <div className={styles.listContent}>{item.desc}</div>
            {/*<Card.Meta description={item.desc} />*/}
          </div>
          <div className={styles.cardRight}>
            <img src="https://cdn.jsdelivr.net/gh/Lavender-z/Lavender-z.github.io/img/blog/5/TopImge/topimage.jpg"
                 alt=""/>
          </div>
        </Card>

      </List.Item>
    )
    }
    bordered
  />
}
export default ListComponent
