import React from 'react'
import styles from './index.less'
import {Avatar, Card, Divider} from 'antd'
import {GithubOutlined, QqOutlined, WechatOutlined} from '@ant-design/icons'

const PersonalInfo = () => {
  return <Card bordered className='box-shadow'>
    <div className={styles.infoHeader}>
      <Avatar
        size={{xs: 24, sm: 32, md: 40, lg: 64, xl: 80, xxl: 100}}
        src='https://img1.baidu.com/it/u=1219077296,1308993239&fm=11&fmt=auto&gp=0.jpg'/>
    </div>
    <div className={styles.infoContent}>
      <p>须灵犀</p>
      <p>琴棋书画、习字、舞</p>
    </div>
    <Divider>社交账号</Divider>
    <div className={styles.infoIcon}>
      <i> <GithubOutlined/></i>
      <i> <QqOutlined/></i>
      <i> <WechatOutlined/></i>
    </div>
  </Card>
}
export default PersonalInfo
