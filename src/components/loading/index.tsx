import styles from './index.less'
import {Spin} from 'antd'
const Loading=()=>{
    return <div className={styles.loadingContainer}>
       <Spin tip='loading...' size='large'/>
    </div>
}
export default Loading
