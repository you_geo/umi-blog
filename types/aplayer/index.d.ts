interface audioOptions {
  name: string;
  artist: string;
  url: string;
  cover: string;
  lrc: string;
}

interface playerOptions {
  container: Element | null,
  fixed: boolean,
  mini: boolean,
  autoplay: boolean,
  lrcType: number,
  audio: Array<audioOptions>
}

declare class aplayer {
  constructor(options: playerOptions);
}


export default aplayer
