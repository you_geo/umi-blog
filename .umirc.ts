import {defineConfig} from 'umi';

const {join} = require('path')
export default defineConfig({
  dynamicImport: {
    loading: '@/components/loading'
  },
  nodeModulesTransform: {
    type: 'none',
  },
  fastRefresh: {},
  mfsu: {},
  webpack5: {},
  chainWebpack(memo) {
    memo.module
      .rule('media')
      .test(/\.(mp[3|4])$/)
      .use('file-loader')
      .loader(require.resolve('file-loader'))
  },
  targets: {
    ie: 11,
  },
  antd: {},
  dva: {
    hmr: true
  },
  proxy: {
    '/api': {
      target: 'http://localhost:1888/',
      changeOrigin: true,
      pathRewrite: {'^/api': ''}
    }
  },
  alias: {
    'utils': join(__dirname, 'src/utils')
  },
  // 修改icon
  links: [
    {rel: 'icon', href: '/001.png'},
  ],
  // 修改title
  title: 'My Blog'
});
